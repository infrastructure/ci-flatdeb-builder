#!/usr/bin/env python3

from pathlib import Path
from typing import Iterable

import binascii
import gzip
import hashlib
import logging
import re
import time

from gi.repository import GLib  # type: ignore
from nacl.encoding import Base64Encoder
from nacl.signing import VerifyKey

import gitlab
import gitlab.v4.objects
import pytest
import requests
import yarl

from conftest import (
    FLATPAK_PRIVATE_KEY,
    FLATPAK_PUBLIC_KEY,
    TEST_BRANCH,
    TestRepo,
    TestRepoBranch,
    TestRepoKind,
)

pytestmark = pytest.mark.gitlab


def _monitor_pipeline_for_completion(pipeline):
    logging.info(f'monitoring pipeline {pipeline.web_url}')
    while pipeline.status in ('running', 'pending', 'created', 'waiting_for_resource'):
        logging.debug(f'pipeline {pipeline.web_url} status is {pipeline.status}')
        pipeline.refresh()
        time.sleep(5)


def _set_ci_variable(project: gitlab.v4.objects.Project, name, value):
    logging.info(f"setting CI/CD variable '{name}' to '{value}'")
    variable = {'key': name, 'value': value}
    try:
        project.variables.update(name, variable)
    except gitlab.GitlabUpdateError:
        project.variables.create(variable)


def _delete_ci_variable(project: gitlab.v4.objects.Project, name):
    logging.info(f"deleting CI/CD variable '{name}'")
    try:
        project.variables.delete(name)
    except gitlab.GitlabDeleteError:
        pass


class GitLabTestVars:
    DISABLE_BRANCH_PIPELINES = 'FLATDEB_BUILDER_TEST_DISABLE_BRANCH_PIPELINES'
    DISABLE_MR_PIPELINES = 'FLATDEB_BUILDER_TEST_DISABLE_MR_PIPELINES'


@pytest.fixture(scope='session')
def gitlab_project(
    gitlab_instance: gitlab.Gitlab,
    test_repo: TestRepo,
    ci_config_project_revision: str,
    ci_config_repo_url: yarl.URL,
    ci_config_file_url: yarl.URL,
    pipeline_id: str,
    disable_signatures: bool,
) -> Iterable[gitlab.v4.objects.Project]:
    project = gitlab_instance.projects.get(test_repo.project_name)

    # If we're not triggering a new pipeline, there's no need to mess with the
    # project.
    if pipeline_id != 'trigger':
        yield project
        return

    logging.info(f'setting up project {test_repo.project_name}')

    project.only_allow_merge_if_pipeline_succeeds = True
    project.merge_requests_access_level = 'enabled'
    project.builds_access_level = 'enabled'
    project.save()

    _set_ci_variable(project, GitLabTestVars.DISABLE_BRANCH_PIPELINES, '1')
    _set_ci_variable(project, GitLabTestVars.DISABLE_MR_PIPELINES, '1')

    if not disable_signatures:
        _set_ci_variable(project, 'FLATPAK_PUBLIC_KEY', FLATPAK_PUBLIC_KEY)
        _set_ci_variable(project, 'FLATPAK_PRIVATE_KEY', FLATPAK_PRIVATE_KEY)
    else:
        _delete_ci_variable(project, 'FLATPAK_PUBLIC_KEY')
        _delete_ci_variable(project, 'FLATPAK_PRIVATE_KEY')

    _set_ci_variable(
        project,
        'UPLOAD_ROOT_MAIN',
        str(test_repo.base_upload_root / 'main'),
    )
    _set_ci_variable(
        project,
        'REMOTE_URL_MAIN',
        str(test_repo.base_url / 'main'),
    )
    _set_ci_variable(
        project,
        'UPLOAD_ROOT_TEST',
        str(test_repo.base_upload_root / 'test'),
    )
    _set_ci_variable(
        project,
        'REMOTE_URL_TEST',
        str(test_repo.base_url / 'test'),
    )

    if test_repo.branch == TestRepoBranch.TEST:
        # Only close other MRs if we're using one ourselves, that way we don't
        # close an MR another test is actively using.
        mrs = project.mergerequests.list(state='opened')
        for mr in mrs:
            mr.state_event = 'close'
            mr.save()

    logging.info(
        f'pointing to the CI definition at {ci_config_repo_url} @ {ci_config_project_revision}'
    )
    project.ci_config_path = str(ci_config_file_url)
    project.save()
    _set_ci_variable(project, 'CI_FLATDEB_BUILDER_REPO', str(ci_config_repo_url))
    _set_ci_variable(project, 'CI_FLATDEB_BUILDER_REVISION', ci_config_project_revision)

    try:
        yield project
    finally:
        _set_ci_variable(project, GitLabTestVars.DISABLE_MR_PIPELINES, '1')


@pytest.fixture(scope='session')
def gitlab_pipeline(
    gitlab_project: gitlab.v4.objects.Project,
    test_repo: TestRepo,
    pipeline_id,
) -> gitlab.v4.objects.ProjectPipeline:
    if pipeline_id != 'trigger':
        pipeline = gitlab_project.pipelines.get(pipeline_id)
    else:
        logging.info(
            'triggering pipeline on'
            f' {test_repo.project_name}:{test_repo.branch.name.lower()}',
        )

        default_branch = gitlab_project.default_branch

        if test_repo.branch == TestRepoBranch.MAIN:
            pipeline = gitlab_project.pipelines.create(
                {
                    'ref': default_branch,
                    'variables': [
                        {
                            'key': GitLabTestVars.DISABLE_BRANCH_PIPELINES,
                            'value': '0',
                        }
                    ],
                }
            )
        else:
            assert test_repo.branch == TestRepoBranch.TEST

            # Need to use -'s here instead of /'s due to:
            # https://gitlab.com/gitlab-org/gitlab/-/issues/208112
            branch = TEST_BRANCH.replace('/', '-')

            try:
                gitlab_project.branches.delete(branch)
            except gitlab.GitlabError as ex:
                # Don't raise an error if the branch didn't exist.
                if ex.response_code != 404:
                    raise

            gitlab_project.commits.create(
                {
                    'branch': branch,
                    'start_branch': default_branch,
                    'commit_message': 'Empty test change',
                    'actions': [],
                }
            )

            mr = gitlab_project.mergerequests.create(
                {
                    'source_branch': branch,
                    'target_branch': default_branch,
                    'title': 'Test MR',
                }
            )

            logging.info(f'created MR {mr.web_url} for {default_branch} -> {branch}')

            _set_ci_variable(gitlab_project, GitLabTestVars.DISABLE_MR_PIPELINES, '0')

            logging.info('creating MR pipeline')
            MAX_RETRIES = 5
            for retry in range(MAX_RETRIES):
                try:
                    mr_pipeline = mr.pipelines.create()
                except gitlab.GitlabCreateError:
                    if retry == MAX_RETRIES - 1:
                        raise
                    time.sleep(1)

            pipeline = gitlab_project.pipelines.get(mr_pipeline.get_id())

    _monitor_pipeline_for_completion(pipeline)
    return pipeline


def _verify_signature_from_metadata(*, to_verify, metadata_bytes):
    metadata = GLib.Variant.new_from_bytes(
        GLib.VariantType.new('a{sv}'), GLib.Bytes.new(metadata_bytes), False
    ).unpack()
    sigs = metadata['ostree.sign.ed25519']
    assert len(sigs) == 1, sigs

    sig = bytes(sigs[0])
    verifier = VerifyKey(FLATPAK_PUBLIC_KEY.encode('ascii'), encoder=Base64Encoder)
    verifier.verify(to_verify, sig)


def _get_pipeline_jobs(
    pipeline: gitlab.v4.objects.ProjectPipeline,
) -> dict[str, gitlab.v4.objects.ProjectPipelineJob]:
    return {j.name: j for j in pipeline.jobs.list(all=True)}  # type: ignore


def _get_pipeline_job_trace(
    project: gitlab.v4.objects.Project,
    job: gitlab.v4.objects.ProjectPipelineJob,
) -> str:
    return project.jobs.get(job.get_id()).trace().decode()  # type: ignore


@pytest.mark.order('first')
def test_gitlab_pipeline(
    test_repo: TestRepo,
    gitlab_project: gitlab.v4.objects.Project,
    gitlab_pipeline: gitlab.v4.objects.ProjectPipeline,
):
    TEST_ARCHITECTURES = {'aarch64', 'arm', 'x86_64'}

    assert gitlab_pipeline.status == 'success', gitlab_pipeline.web_url

    parent_jobs = _get_pipeline_jobs(gitlab_pipeline)
    assert set(parent_jobs) == {'generate-pipeline'}

    bridges = {b.name: b for b in gitlab_pipeline.bridges.list()}
    assert set(bridges) == {'run-pipeline'}

    build_pipeline = gitlab_project.pipelines.get(
        bridges['run-pipeline'].downstream_pipeline['id']
    )
    jobs = _get_pipeline_jobs(build_pipeline)

    if test_repo.kind == TestRepoKind.RUNTIMES:
        assert set(jobs) == {
            'build base (aarch64)',
            'build base (arm)',
            'build base (x86_64)',
            'build runtime org.apertis.headless (aarch64)',
            'build runtime org.apertis.headless (arm)',
            'build runtime org.apertis.headless (x86_64)',
            'build runtime org.apertis.curl (aarch64)',
            'build runtime org.apertis.curl (arm)',
            'build runtime org.apertis.curl (x86_64)',
            'build app org.apertis.curl.hello (aarch64)',
            'build app org.apertis.curl.hello (arm)',
            'build app org.apertis.curl.hello (x86_64)',
            'build app org.apertis.headless.hello (aarch64)',
            'build app org.apertis.headless.hello (arm)',
            'build app org.apertis.headless.hello (x86_64)',
            'test runtimes (aarch64)',
            'test runtimes (arm)',
            'test runtimes (x86_64)',
            'publish runtime org.apertis.headless (aarch64)',
            'publish runtime org.apertis.headless (arm)',
            'publish runtime org.apertis.headless (x86_64)',
            'publish runtime org.apertis.curl (aarch64)',
            'publish runtime org.apertis.curl (arm)',
            'publish runtime org.apertis.curl (x86_64)',
            'publish app org.apertis.headless.hello (aarch64)',
            'publish app org.apertis.headless.hello (arm)',
            'publish app org.apertis.headless.hello (x86_64)',
            'publish app org.apertis.curl.hello (aarch64)',
            'publish app org.apertis.curl.hello (arm)',
            'publish app org.apertis.curl.hello (x86_64)',
            'publish flatpakrepo',
        }

        for arch in TEST_ARCHITECTURES:
            job = jobs[f'test runtimes ({arch})']
            trace = _get_pipeline_job_trace(gitlab_project, job)

            assert 'ls: /usr/bin/ls' in trace
            assert 'curl: /usr/bin/curl' in trace
    else:
        assert test_repo.kind == TestRepoKind.APPS

        assert set(jobs) == {
            'build app org.apertis.builder.headless.hello (aarch64)',
            'build app org.apertis.builder.headless.hello (arm)',
            'build app org.apertis.builder.headless.hello (x86_64)',
            'build app org.apertis.builder.hmi.hello (aarch64)',
            'build app org.apertis.builder.hmi.hello (arm)',
            'build app org.apertis.builder.hmi.hello (x86_64)',
            'test headless (aarch64)',
            'test headless (arm)',
            'test headless (x86_64)',
            'test hmi (aarch64)',
            'test hmi (arm)',
            'test hmi (x86_64)',
            'publish app org.apertis.builder.headless.hello (aarch64)',
            'publish app org.apertis.builder.headless.hello (arm)',
            'publish app org.apertis.builder.headless.hello (x86_64)',
            'publish app org.apertis.builder.hmi.hello (aarch64)',
            'publish app org.apertis.builder.hmi.hello (arm)',
            'publish app org.apertis.builder.hmi.hello (x86_64)',
        }

        for arch in TEST_ARCHITECTURES:
            job = jobs[f'test headless ({arch})']
            trace = _get_pipeline_job_trace(gitlab_project, job)
            assert '##### headless: Hello, user!' in trace, job.web_url

            job = jobs[f'test hmi ({arch})']
            trace = _get_pipeline_job_trace(gitlab_project, job)
            assert re.search(r'Hello user, from fontconfig \d+!', trace), job.web_url


def _get(url: yarl.URL) -> requests.Response:
    response = requests.get(str(url))
    response.raise_for_status()
    return response


def _parse_variant(vtype: str, data: bytes):
    return GLib.Variant.new_from_bytes(
        GLib.VariantType.new(vtype),
        GLib.Bytes.new(data),
        False,
    ).unpack()


@pytest.fixture(scope='session')
def summary_idx_bytes(test_repo: TestRepo) -> bytes:
    return _get(test_repo.repo_url / 'summary.idx').content


@pytest.fixture(scope='session')
def summary_idx(summary_idx_bytes: bytes):
    return _parse_variant('(a{s(ayaaya{sv})}a{sv})', summary_idx_bytes)


@pytest.fixture(scope='session')
def subsummaries(test_repo: TestRepo, summary_idx):
    results = []

    for arch, (subsummary_checksum, *_) in summary_idx[0].items():
        subsummary_id = binascii.hexlify(bytes(subsummary_checksum)).decode()
        logging.info('downloading subsummary %s for %s', subsummary_id, arch)
        subsummary_bytes_gz = _get(
            test_repo.repo_url / 'summaries' / f'{subsummary_id}.gz'
        ).content
        subsummary_bytes = gzip.decompress(subsummary_bytes_gz)
        results.append(
            (arch, _parse_variant('(a(s(taya{sv}))a{sv})', subsummary_bytes))
        )

    return results


@pytest.fixture(scope='session')
def summary_refs(subsummaries):
    result = set()

    for (arch, subsummary) in subsummaries:
        refs, _meta = subsummary
        for ref, _ in refs:
            if ref.startswith(('runtime/', 'app/')):
                result.add(ref)

    return result


def test_summary_signed(
    test_repo: TestRepo,
    summary_idx_bytes: bytes,
    disable_signatures: bool,
):
    if disable_signatures:
        pytest.skip('signatures are disabled')

    checksum = hashlib.sha256(summary_idx_bytes).hexdigest()
    sig_metadata = _get(
        test_repo.repo_url / 'summaries' / f'{checksum}.idx.sig'
    ).content
    _verify_signature_from_metadata(
        to_verify=summary_idx_bytes,
        metadata_bytes=sig_metadata,
    )


def test_subsummaries_refs_correct(test_repo: TestRepo, summary_refs):
    if test_repo.kind == TestRepoKind.RUNTIMES:
        assert summary_refs == {
            f'app/org.apertis.curl.hello/aarch64/v2022',
            f'app/org.apertis.curl.hello/arm/v2022',
            f'app/org.apertis.curl.hello/x86_64/v2022',
            f'app/org.apertis.headless.hello/aarch64/v2022',
            f'app/org.apertis.headless.hello/arm/v2022',
            f'app/org.apertis.headless.hello/x86_64/v2022',
            f'runtime/org.apertis.curl.Platform/aarch64/v2022',
            f'runtime/org.apertis.curl.Platform/arm/v2022',
            f'runtime/org.apertis.curl.Platform/x86_64/v2022',
            f'runtime/org.apertis.curl.Platform.Locale/aarch64/v2022',
            f'runtime/org.apertis.curl.Platform.Locale/arm/v2022',
            f'runtime/org.apertis.curl.Platform.Locale/x86_64/v2022',
            f'runtime/org.apertis.curl.Sdk/aarch64/v2022',
            f'runtime/org.apertis.curl.Sdk/arm/v2022',
            f'runtime/org.apertis.curl.Sdk/x86_64/v2022',
            f'runtime/org.apertis.curl.Sdk.Debug/aarch64/v2022',
            f'runtime/org.apertis.curl.Sdk.Debug/arm/v2022',
            f'runtime/org.apertis.curl.Sdk.Debug/x86_64/v2022',
            f'runtime/org.apertis.curl.Sdk.Locale/aarch64/v2022',
            f'runtime/org.apertis.curl.Sdk.Locale/arm/v2022',
            f'runtime/org.apertis.curl.Sdk.Locale/x86_64/v2022',
            f'runtime/org.apertis.curl.Sdk.Sources/aarch64/v2022',
            f'runtime/org.apertis.curl.Sdk.Sources/arm/v2022',
            f'runtime/org.apertis.curl.Sdk.Sources/x86_64/v2022',
            f'runtime/org.apertis.curl.hello.Sources/aarch64/v2022',
            f'runtime/org.apertis.curl.hello.Sources/arm/v2022',
            f'runtime/org.apertis.curl.hello.Sources/x86_64/v2022',
            f'runtime/org.apertis.headless.Platform/aarch64/v2022',
            f'runtime/org.apertis.headless.Platform/arm/v2022',
            f'runtime/org.apertis.headless.Platform/x86_64/v2022',
            f'runtime/org.apertis.headless.Platform.Locale/aarch64/v2022',
            f'runtime/org.apertis.headless.Platform.Locale/arm/v2022',
            f'runtime/org.apertis.headless.Platform.Locale/x86_64/v2022',
            f'runtime/org.apertis.headless.Sdk/aarch64/v2022',
            f'runtime/org.apertis.headless.Sdk/arm/v2022',
            f'runtime/org.apertis.headless.Sdk/x86_64/v2022',
            f'runtime/org.apertis.headless.Sdk.Debug/aarch64/v2022',
            f'runtime/org.apertis.headless.Sdk.Debug/arm/v2022',
            f'runtime/org.apertis.headless.Sdk.Debug/x86_64/v2022',
            f'runtime/org.apertis.headless.Sdk.Locale/aarch64/v2022',
            f'runtime/org.apertis.headless.Sdk.Locale/arm/v2022',
            f'runtime/org.apertis.headless.Sdk.Locale/x86_64/v2022',
            f'runtime/org.apertis.headless.Sdk.Sources/aarch64/v2022',
            f'runtime/org.apertis.headless.Sdk.Sources/arm/v2022',
            f'runtime/org.apertis.headless.Sdk.Sources/x86_64/v2022',
            f'runtime/org.apertis.headless.hello.Sources/aarch64/v2022',
            f'runtime/org.apertis.headless.hello.Sources/arm/v2022',
            f'runtime/org.apertis.headless.hello.Sources/x86_64/v2022',
        }
    else:
        assert test_repo.kind == TestRepoKind.APPS

        branch = {
            TestRepoBranch.MAIN: 'stable',
            TestRepoBranch.TEST: 'test',
        }[test_repo.branch]

        assert summary_refs == {
            f'app/org.apertis.builder.headless.hello/aarch64/{branch}',
            f'app/org.apertis.builder.headless.hello/arm/{branch}',
            f'app/org.apertis.builder.headless.hello/x86_64/{branch}',
            f'app/org.apertis.builder.hmi.hello/aarch64/{branch}',
            f'app/org.apertis.builder.hmi.hello/arm/{branch}',
            f'app/org.apertis.builder.hmi.hello/x86_64/{branch}',
            f'runtime/org.apertis.builder.headless.hello.Debug/aarch64/{branch}',
            f'runtime/org.apertis.builder.headless.hello.Debug/arm/{branch}',
            f'runtime/org.apertis.builder.headless.hello.Debug/x86_64/{branch}',
            f'runtime/org.apertis.builder.headless.hello.Sources/aarch64/{branch}',
            f'runtime/org.apertis.builder.headless.hello.Sources/arm/{branch}',
            f'runtime/org.apertis.builder.headless.hello.Sources/x86_64/{branch}',
            f'runtime/org.apertis.builder.hmi.hello.Debug/aarch64/{branch}',
            f'runtime/org.apertis.builder.hmi.hello.Debug/arm/{branch}',
            f'runtime/org.apertis.builder.hmi.hello.Debug/x86_64/{branch}',
            f'runtime/org.apertis.builder.hmi.hello.Sources/aarch64/{branch}',
            f'runtime/org.apertis.builder.hmi.hello.Sources/arm/{branch}',
            f'runtime/org.apertis.builder.hmi.hello.Sources/x86_64/{branch}',
        }


def test_subsummaries_refs_signed(
    test_repo: TestRepo,
    subsummaries,
    disable_signatures: bool,
):
    if disable_signatures:
        pytest.skip('signatures are disabled')

    for (_arch, subsummary) in subsummaries:
        refs, _meta = subsummary

        for ref, (_commit_size, commit_checksum, _ref_metadata) in refs:
            logging.info('ref %s', ref)

            expected_commit = binascii.hexlify(bytes(commit_checksum)).decode()
            commit = _get(test_repo.repo_url / 'refs' / 'heads' / ref).text.strip()
            assert commit == expected_commit

            commit_directory = test_repo.repo_url / 'objects' / commit[0:2]
            commit_basename = commit[2:]

            commit_contents = _get(
                commit_directory / f'{commit_basename}.commit'
            ).content
            commit_metadata = _get(
                commit_directory / f'{commit_basename}.commitmeta'
            ).content

            _verify_signature_from_metadata(
                to_verify=commit_contents,
                metadata_bytes=commit_metadata,
            )


def test_subsummaries_flatpak_metadata(subsummaries):
    for (_arch, subsummary) in subsummaries:
        refs, _meta = subsummary

        for ref, (_commit_size, _commit_checksum, ref_metadata) in refs:
            if ref.startswith('runtime/'):
                group = 'Runtime'
            elif ref.startswith('app/'):
                group = 'Application'
            elif ref.startswith(('appstream/', 'appstream2/')):
                continue
            else:
                assert False, ref

            flatpak_metadata = ref_metadata['xa.data'][2]

            # The metadata file should have a single trailing newline.
            assert flatpak_metadata.endswith('\n'), ref
            assert not flatpak_metadata.endswith('\n\n'), ref

            keyfile = GLib.KeyFile.new()
            keyfile.load_from_bytes(GLib.Bytes.new(flatpak_metadata.encode('ascii')), 0)

            ref_id = ref.split('/')[1]
            id_from_metadata = keyfile.get_string(group, 'name')
            assert ref_id == id_from_metadata


def test_flatpakrepo(test_repo: TestRepo, disable_signatures: bool):
    if not test_repo.kind.has_flatpakrepo:
        pytest.skip('repository does not have a .flatpakrepo')

    flatpakrepo = _get(test_repo.repo_url / 'apertis.flatpakrepo').content

    keyfile = GLib.KeyFile.new()
    keyfile.load_from_bytes(GLib.Bytes.new(flatpakrepo), 0)

    assert keyfile.get_string('Flatpak Repo', 'Url') == str(test_repo.repo_url)
    assert keyfile.get_string('Flatpak Repo', 'SignatureType') == 'ed25519'

    expected_key = FLATPAK_PUBLIC_KEY if not disable_signatures else ''
    assert keyfile.get_string('Flatpak Repo', 'SignatureKey') == expected_key
