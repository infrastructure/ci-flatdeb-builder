from dataclasses import dataclass
from pathlib import Path
from typing import Any, ClassVar

import sys
import textwrap

from approvaltests.approvals import verify  # type: ignore
from approvaltests.namer import NamerFactory  # type: ignore

import pytest

from generate_pipeline import (
    FLATDEB_BUILDER_CONFIG_PATH,
    FlatdebBuilderConfig,
    dump_yaml,
    main,
)

from conftest import FLATPAK_PRIVATE_KEY, FLATPAK_PUBLIC_KEY, TestRepoBranch


@pytest.fixture(
    params=list(FlatdebBuilderConfig.StableBranches),
    ids=[v.value for v in FlatdebBuilderConfig.StableBranches],
)
def stable_branches(request):
    return request.param


@dataclass
class FixedTestOptions:
    VALUES: ClassVar[list['FixedTestOptions']] = []

    sign: bool
    tests: bool
    legacypush: bool
    flatpakrepo: bool
    deps_sign: FlatdebBuilderConfig.InstallDepsFrom.Sign

    @property
    def pytest_id(self) -> str:
        def with_(name, value):
            return f'with_{name}' if value else f'without_{name}'

        return '-'.join(
            (
                with_('sign', self.sign),
                with_('tests', self.tests),
                with_('legacypush', self.legacypush),
                with_('flatpakrepo', self.flatpakrepo),
                f'deps_sign_{self.deps_sign.value}',
            )
        )


FixedTestOptions.VALUES = [
    FixedTestOptions(
        sign=True,
        tests=True,
        legacypush=False,
        flatpakrepo=True,
        deps_sign=FlatdebBuilderConfig.InstallDepsFrom.Sign.NONE,
    ),
    FixedTestOptions(
        sign=False,
        tests=True,
        legacypush=False,
        flatpakrepo=True,
        deps_sign=FlatdebBuilderConfig.InstallDepsFrom.Sign.ED25519,
    ),
    FixedTestOptions(
        sign=False,
        tests=False,
        legacypush=True,
        flatpakrepo=False,
        deps_sign=FlatdebBuilderConfig.InstallDepsFrom.Sign.GPG,
    ),
]


@pytest.fixture(
    params=FixedTestOptions.VALUES,
    ids=[v.pytest_id for v in FixedTestOptions.VALUES],
)
def fixed_test_options(request) -> FixedTestOptions:
    return request.param


@pytest.fixture(autouse=True)
def setup(
    test_repo_branch: TestRepoBranch,
    stable_branches: FlatdebBuilderConfig.StableBranches,
    fixed_test_options: FixedTestOptions,
    tmp_path: Path,
    monkeypatch: pytest.MonkeyPatch,
):
    monkeypatch.setattr(sys, 'argv', ['./generate_pipelines.py'])
    monkeypatch.chdir(tmp_path)

    monkeypatch.setenv('OSNAME', 'apertis')
    monkeypatch.setenv('APERTIS_STABLE_RELEASE_DEFAULT', 'v2022')
    monkeypatch.setenv('DOCKER_IMAGE_TEMPLATE', 'mystery-registry:{release}')
    monkeypatch.setenv('UPLOAD_HOST', 'user@upload-host')
    monkeypatch.setenv('REMOTE_URL_MAIN', 'https://upload-host.xyz/main')
    monkeypatch.setenv('REMOTE_URL_TEST', 'https://upload-host.xyz/test')
    monkeypatch.setenv('UPLOAD_ROOT_MAIN', '/srv/main')
    monkeypatch.setenv('UPLOAD_ROOT_TEST', '/srv/test')
    monkeypatch.setenv('ARCHIVE_SECRET_FILE', 'ssh-key')
    monkeypatch.setenv('QEMU_RUNNER_TAG', 'kvm')

    monkeypatch.setenv(
        'CI_COMMIT_BRANCH',
        {
            FlatdebBuilderConfig.StableBranches.VERSIONED: test_repo_branch.versioned_git_branch,
            FlatdebBuilderConfig.StableBranches.MAIN_ONLY: test_repo_branch.main_only_git_branch,
        }[stable_branches],
    )

    if stable_branches == FlatdebBuilderConfig.StableBranches.VERSIONED:
        monkeypatch.setenv('APERTIS_RELEASE_DEFAULT', 'v2023')

    if fixed_test_options.sign:
        monkeypatch.setenv('FLATPAK_PUBLIC_KEY', FLATPAK_PUBLIC_KEY)
        monkeypatch.setenv('FLATPAK_PRIVATE_KEY', FLATPAK_PRIVATE_KEY)

    if fixed_test_options.legacypush:
        monkeypatch.setenv('OSTREE_PUSH_LEGACY_RELEASE_PATTERN', r'v\d{4}')
    else:
        monkeypatch.setenv('OSTREE_PUSH_LEGACY_RELEASE_PATTERN', 'thismatchesnothing')
        monkeypatch.setenv('OSTREE_PUSH_SERVER_CONFIG', '/opt/ostree-push/test.conf')


@pytest.fixture
def config(
    stable_branches: FlatdebBuilderConfig.StableBranches,
    fixed_test_options: FixedTestOptions,
):
    config: dict[str, Any] = {
        'stable_branches': stable_branches.value,
    }

    if fixed_test_options.flatpakrepo:
        config['flatpakrepo'] = {
            'name': 'testrepo',
            'template': textwrap.dedent(
                """\
                [Flatpak Repo]
                Title=Test Repo
                Url=$REMOTE_URL
                SignatureKey=$FLATPAK_PUBLIC_KEY
                SignatureType=ed25519
            """.rstrip()
            ),
        }

    return config


@dataclass
class _Verifier:
    capfd: pytest.CaptureFixture[str]
    approval_parameters: list[str]

    def run(self) -> None:
        main()
        out, _ = self.capfd.readouterr()
        verify(
            out,
            options=NamerFactory.with_parameters(
                *self.approval_parameters
            ).for_file.with_extension('.yaml'),
        )


@pytest.fixture
@pytest.mark.usefixtures('setup')
def verifier(
    capfd: pytest.CaptureFixture[str], request: pytest.FixtureRequest
) -> _Verifier:
    assert isinstance(request.node, pytest.Function)
    return _Verifier(
        capfd=capfd,
        approval_parameters=request.node.callspec.id.split('-'),
    )


def test_runtimes(
    config: dict[str, Any],
    # Make sure stable_branches comes before the options in the approval test filename.
    stable_branches: FlatdebBuilderConfig.StableBranches,
    fixed_test_options: FixedTestOptions,
    verifier: _Verifier,
):
    if fixed_test_options.tests:
        config['tests'] = {
            'test1': {
                'command': 'echo 123',
                'targets': {'apps': ['org.test.foo.Test']},
            },
            'test2': {
                'command': 'echo 123',
                'targets': {
                    'runtimes': ['org.test.foo.Platform'],
                    'apps': ['org.test.bar.Test'],
                },
            },
        }

    with FLATDEB_BUILDER_CONFIG_PATH.open('w') as fp:
        dump_yaml(config, to=fp)

    runtimes = Path('runtimes')
    runtimes.mkdir()

    with (runtimes / 'foo.yaml').open('w') as fp:
        dump_yaml({'id_prefix': 'org.test.foo'}, to=fp)
    with (runtimes / 'bar.yaml').open('w') as fp:
        dump_yaml({'id_prefix': 'org.test.bar'}, to=fp)

    apps = Path('apps')
    apps.mkdir()

    with (apps / 'recipe1.yaml').open('w') as fp:
        dump_yaml(
            {
                'app-id': 'org.test.foo.Test',
                'runtime': 'org.test.foo.Platform',
                'sdk': 'org.test.foo.Sdk',
            },
            to=fp,
        )

    with (apps / 'recipe2.yaml').open('w') as fp:
        dump_yaml(
            {
                'app-id': 'org.test.bar.Test',
                'runtime': 'org.test.bar.Platform',
                'sdk': 'org.test.bar.Sdk',
            },
            to=fp,
        )

    verifier.run()


def test_apps(
    config: dict[str, Any],
    # Make sure stable_branches comes before the options in the approval test filename.
    stable_branches: FlatdebBuilderConfig.StableBranches,
    fixed_test_options: FixedTestOptions,
    verifier: _Verifier,
):
    config['install_deps_from'] = {
        'url': 'https://images.apertis.org/flatpak/repo/apertis.flatpakrepo',
        'sign': fixed_test_options.deps_sign.value,
    }

    if fixed_test_options.tests:
        config['tests'] = {
            'test1': {
                'command': 'echo 123',
                'targets': {'apps': ['org.test.Test_1']},
            },
            'test2': {
                'command': 'echo 123',
                'targets': {'apps': ['org.test.Test_2']},
            },
        }

    with FLATDEB_BUILDER_CONFIG_PATH.open('w') as fp:
        dump_yaml(config, to=fp)

    with open('recipe1.yaml', 'w') as fp:
        dump_yaml(
            {
                'app-id': 'org.test.Test-1',
                'runtime': 'org.test.Platform',
                'runtime-version': '10',
                'sdk': 'org.test.Sdk',
            },
            to=fp,
        )

    with open('recipe2.yaml', 'w') as fp:
        dump_yaml(
            {
                'app-id': 'org.test.Test-2',
                'runtime': 'org.test2.Platform',
                'runtime-version': '50',
                'sdk': 'org.test2.Sdk',
            },
            to=fp,
        )

    verifier.run()
