#!/bin/bash

tests_dir=$(dirname "$0")
cd "$tests_dir/approved_files"

for received in *.received.yaml; do
  cp "$received" "${received%*.received.yaml}".approved.yaml
done
