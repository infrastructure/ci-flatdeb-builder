from pathlib import Path

import pydantic
import pytest

from generate_pipeline import (
    Arch,
    ArchlessRef,
    BranchInfo,
    BuildAction,
    BuildActionKind,
    FlatdebBuilderConfig,
    OstreePush,
    RefKind,
    SigningKeyPair,
    UploadInfo,
    check_actions_for_duplicate_refs,
    dump_yaml,
    get_release_from_git_branch,
    parse_builder_manifest,
    parse_flatdeb_manifest,
    recursively_gather_deps,
)


def test_config_parsing():
    config = FlatdebBuilderConfig.parse_obj(
        {
            'supported_architectures': ['aarch64'],
            'stable_branches': 'main_only',
            'install_deps_from': {
                'url': 'https://place/',
                'sign': 'gpg',
            },
            'tests': {
                'thing': {
                    'command': 'echo',
                    'targets': {
                        'runtimes': ['a'],
                        'apps': ['b'],
                    },
                },
            },
            'flatpakrepo': {
                'name': 'myrepo',
                'template': '$REMOTE_URL : $FLATPAK_PUBLIC_KEY',
            },
        }
    )

    assert config.supported_architectures == [
        a for a in Arch.VALUES if a.flatpak_arch == 'aarch64'
    ]
    assert config.stable_branches == FlatdebBuilderConfig.StableBranches.MAIN_ONLY
    assert config.install_deps_from is not None
    assert config.install_deps_from.url == 'https://place/'
    assert (
        config.install_deps_from.sign == FlatdebBuilderConfig.InstallDepsFrom.Sign.GPG
    )
    assert config.tests == {
        'thing': FlatdebBuilderConfig.TestJob(
            command='echo',
            targets=FlatdebBuilderConfig.TestJob.Targets(runtimes={'a'}, apps={'b'}),
        )
    }
    assert config.flatpakrepo is not None
    assert config.flatpakrepo.name == 'myrepo'
    assert config.flatpakrepo.template == '$REMOTE_URL : $FLATPAK_PUBLIC_KEY'


def test_config_validates_flatpakrepo_template():
    with pytest.raises(pydantic.ValidationError, match='undefined variable'):
        FlatdebBuilderConfig.parse_obj(
            {
                'flatpakrepo': {
                    'name': 'test',
                    'template': '$Z',
                }
            }
        )


def test_signing_key_pair_none():
    assert SigningKeyPair.from_env(is_legacy_ostree_push=False) is None


def test_signing_key_pair_success(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('FLATPAK_PUBLIC_KEY', '123')
    monkeypatch.setenv('FLATPAK_PRIVATE_KEY', '456')

    keypair = SigningKeyPair.from_env(is_legacy_ostree_push=False)
    assert keypair is not None
    assert keypair.public == '123'
    assert keypair.private == '456'


@pytest.mark.parametrize('env', ['FLATPAK_PUBLIC_KEY', 'FLATPAK_PRIVATE_KEY'])
def test_signing_key_pair_errors_if_one_missing(
    env: str, monkeypatch: pytest.MonkeyPatch
):
    monkeypatch.setenv(env, '123')
    with pytest.raises(ValueError, match='both be set'):
        SigningKeyPair.from_env(is_legacy_ostree_push=False)


@pytest.fixture(params=['CI_MERGE_REQUEST_TARGET_BRANCH_NAME', 'CI_COMMIT_BRANCH'])
def branch_env_var(request):
    return request.param


def test_release_from_git_branch():
    assert get_release_from_git_branch('apertis/v2022') == 'v2022'
    assert get_release_from_git_branch('apertis/v2023dev3') == 'v2023dev3'
    assert get_release_from_git_branch('apertis/v2023dev3-test') == 'v2023dev3'
    assert get_release_from_git_branch('apertis/test-v2023pre') == 'v2023pre'
    assert get_release_from_git_branch('av2023pre') is None
    assert get_release_from_git_branch('v2022foo') is None


def test_branch_info_stable_main(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('APERTIS_STABLE_RELEASE_DEFAULT', 'v2022')
    monkeypatch.setenv('DOCKER_IMAGE_TEMPLATE', 'myimage:{release}')
    monkeypatch.setenv('CI_COMMIT_BRANCH', 'main')

    info = BranchInfo.from_env(FlatdebBuilderConfig.StableBranches.MAIN_ONLY)
    assert info.is_stable
    assert info.flatpak_branch == 'stable'
    assert info.os_release == 'v2022'
    assert info.docker_image == 'myimage:v2022'


def test_branch_info_mr_main(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('APERTIS_STABLE_RELEASE_DEFAULT', 'v2022')
    monkeypatch.setenv('DOCKER_IMAGE_TEMPLATE', 'myimage:{release}')
    monkeypatch.setenv('CI_MERGE_REQUEST_TARGET_BRANCH_NAME', 'main')

    info = BranchInfo.from_env(FlatdebBuilderConfig.StableBranches.MAIN_ONLY)
    assert not info.is_stable
    assert info.flatpak_branch == 'test'
    assert info.os_release == 'v2022'
    assert info.docker_image == 'myimage:v2022'


def test_branch_info_unstable_main(
    branch_env_var: str, monkeypatch: pytest.MonkeyPatch
):
    monkeypatch.setenv('APERTIS_STABLE_RELEASE_DEFAULT', 'v2022')
    monkeypatch.setenv('DOCKER_IMAGE_TEMPLATE', 'myimage:{release}')
    monkeypatch.setenv(branch_env_var, 'wip/xyz')

    info = BranchInfo.from_env(FlatdebBuilderConfig.StableBranches.MAIN_ONLY)
    assert not info.is_stable
    assert info.flatpak_branch == 'test'
    assert info.os_release == 'v2022'
    assert info.docker_image == 'myimage:v2022'


def test_branch_info_stable_versioned(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('DOCKER_IMAGE_TEMPLATE', 'myimage:{release}')
    monkeypatch.setenv('CI_COMMIT_BRANCH', 'apertis/v2022')
    monkeypatch.setenv('OSNAME', 'apertis')

    info = BranchInfo.from_env(FlatdebBuilderConfig.StableBranches.VERSIONED)
    assert info.is_stable
    assert info.flatpak_branch == 'v2022'
    assert info.os_release == 'v2022'
    assert info.docker_image == 'myimage:v2022'


def test_branch_info_mr_versioned(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('DOCKER_IMAGE_TEMPLATE', 'myimage:{release}')
    monkeypatch.setenv('CI_MERGE_REQUEST_TARGET_BRANCH_NAME', 'apertis/v2022')
    monkeypatch.setenv('OSNAME', 'apertis')

    info = BranchInfo.from_env(FlatdebBuilderConfig.StableBranches.VERSIONED)
    assert not info.is_stable
    assert info.flatpak_branch == 'v2022'
    assert info.os_release == 'v2022'
    assert info.docker_image == 'myimage:v2022'


def test_branch_info_unstable_versioned_uses_default(
    branch_env_var: str, monkeypatch: pytest.MonkeyPatch
):
    monkeypatch.setenv('APERTIS_RELEASE_DEFAULT', 'v2022')
    monkeypatch.setenv('DOCKER_IMAGE_TEMPLATE', 'myimage:{release}')
    monkeypatch.setenv(branch_env_var, 'wip/xyz')
    monkeypatch.setenv('OSNAME', 'apertis')

    info = BranchInfo.from_env(FlatdebBuilderConfig.StableBranches.VERSIONED)
    assert not info.is_stable
    assert info.flatpak_branch == 'v2022'
    assert info.os_release == 'v2022'
    assert info.docker_image == 'myimage:v2022'


def test_upload_info_no_key(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('REMOTE_URL_MAIN', 'https://localhost/main')
    monkeypatch.setenv('UPLOAD_ROOT_MAIN', '/srv/host/main')
    monkeypatch.setenv('UPLOAD_HOST', 'user@host')

    info = UploadInfo.from_env(is_legacy_ostree_push=False, is_stable=True)
    assert info is None


def test_upload_info_stable(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('REMOTE_URL_MAIN', 'https://localhost/main')
    monkeypatch.setenv('UPLOAD_ROOT_MAIN', '/srv/host/main')
    monkeypatch.setenv('UPLOAD_HOST', 'user@host')
    monkeypatch.setenv('ARCHIVE_SECRET_FILE', 'test-key')

    info = UploadInfo.from_env(is_legacy_ostree_push=False, is_stable=True)
    assert info is not None
    assert info.ssh_key == Path('test-key')
    assert info.remote_url == 'https://localhost/main'
    assert info.root == '/srv/host/main'
    assert info.ssh_host == 'user@host'


def test_upload_info_unstable(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('REMOTE_URL_TEST', 'https://localhost/test')
    monkeypatch.setenv('UPLOAD_ROOT_TEST', '/srv/host/test')
    monkeypatch.setenv('UPLOAD_HOST', 'user@host')
    monkeypatch.setenv('ARCHIVE_SECRET_FILE', 'test-key')

    info = UploadInfo.from_env(is_legacy_ostree_push=False, is_stable=False)
    assert info is not None
    assert info.ssh_key == Path('test-key')
    assert info.remote_url == 'https://localhost/test'
    assert info.root == '/srv/host/test'
    assert info.ssh_host == 'user@host'


def test_upload_info_port(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('REMOTE_URL_MAIN', 'https://localhost/main')
    monkeypatch.setenv('UPLOAD_ROOT_MAIN', '/srv/host/main')
    monkeypatch.setenv('UPLOAD_HOST', 'user@host')
    monkeypatch.setenv('UPLOAD_PORT', '22')
    monkeypatch.setenv('ARCHIVE_SECRET_FILE', 'test-key')

    info = UploadInfo.from_env(is_legacy_ostree_push=False, is_stable=True)
    assert info is not None
    assert f'-p22' in info.ssh_flags


def test_ostree_push_modern(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('OSTREE_PUSH_SERVER_CONFIG', '/etc/config')
    monkeypatch.setenv('OSTREE_PUSH_LEGACY_RELEASE_PATTERN', 'v202[123]')

    ostree_push = OstreePush.from_env(os_release='v2024')
    assert not ostree_push.is_legacy
    assert ostree_push.config == '/etc/config'


def test_ostree_push_legacy(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv('OSTREE_PUSH_SERVER_CONFIG', '/etc/config')
    monkeypatch.setenv('OSTREE_PUSH_LEGACY_RELEASE_PATTERN', 'v202[123]')

    ostree_push = OstreePush.from_env(os_release='v2022')
    assert ostree_push.is_legacy
    assert ostree_push.config is None


def test_parse_flatdeb_manifest(tmp_path: Path):
    manifest = tmp_path / 'test.yaml'
    with manifest.open('w') as fp:
        dump_yaml({'id_prefix': 'org.test'}, to=fp)

    info = parse_flatdeb_manifest(manifest, 'v2022')
    assert info.id_prefix == 'org.test'
    assert info.refs == {
        ArchlessRef(RefKind.RUNTIME, 'org.test.Platform', 'v2022'),
        ArchlessRef(RefKind.RUNTIME, 'org.test.Sdk', 'v2022'),
    }


def test_parse_flatdeb_manifest_missing_id_prefix(tmp_path: Path):
    manifest = tmp_path / 'test.yaml'
    with manifest.open('w') as fp:
        dump_yaml({'id_prefix_x': 'org.test'}, to=fp)

    with pytest.raises(ValueError, match='Missing id_prefix'):
        parse_flatdeb_manifest(manifest, 'v2022')


@pytest.mark.parametrize('id_key', ['app-id', 'id'])
def test_parse_builder_manifest(id_key: str, tmp_path: Path):
    manifest = tmp_path / 'test.yaml'
    with manifest.open('w') as fp:
        dump_yaml(
            {
                id_key: 'org.test.Test',
                'runtime': 'org.test.Platform',
                'runtime-version': '20',
                'sdk': 'org.test.Sdk',
            },
            to=fp,
        )

    info = parse_builder_manifest(manifest, 'v2022')
    assert info.ref == ArchlessRef(RefKind.APP, 'org.test.Test', 'v2022')
    assert info.deps == {
        ArchlessRef(RefKind.RUNTIME, 'org.test.Platform', '20'),
        ArchlessRef(RefKind.RUNTIME, 'org.test.Sdk', '20'),
    }


@pytest.mark.parametrize('id_key', ['app-id', 'id'])
def test_parse_builder_manifest_default_runtime_version(id_key: str, tmp_path: Path):
    manifest = tmp_path / 'test.yaml'
    with manifest.open('w') as fp:
        dump_yaml(
            {
                id_key: 'org.test.Test',
                'runtime': 'org.test.Platform',
                'sdk': 'org.test.Sdk',
            },
            to=fp,
        )

    info = parse_builder_manifest(manifest, 'v2022')
    assert info.ref == ArchlessRef(RefKind.APP, 'org.test.Test', 'v2022')
    assert info.deps == {
        ArchlessRef(RefKind.RUNTIME, 'org.test.Platform', 'v2022'),
        ArchlessRef(RefKind.RUNTIME, 'org.test.Sdk', 'v2022'),
    }


@pytest.mark.parametrize('id_key', ['app-id', 'id'])
def test_parse_builder_manifest_extension(id_key: str, tmp_path: Path):
    manifest = tmp_path / 'test.yaml'
    with manifest.open('w') as fp:
        dump_yaml(
            {
                id_key: 'org.test.Test',
                'runtime': 'org.test.Platform',
                'runtime-version': '20',
                'sdk': 'org.test.Sdk',
                'build-extension': True,
            },
            to=fp,
        )

    info = parse_builder_manifest(manifest, 'v2022')
    assert info.ref == ArchlessRef(RefKind.RUNTIME, 'org.test.Test', 'v2022')
    assert info.deps == {
        ArchlessRef(RefKind.RUNTIME, 'org.test.Platform', '20'),
        ArchlessRef(RefKind.RUNTIME, 'org.test.Sdk', '20'),
    }


def test_parse_builder_manifest_missing_keys(tmp_path):
    manifest = tmp_path / 'test.yaml'
    with manifest.open('w') as fp:
        dump_yaml({}, to=fp)

    with pytest.raises(ValueError, match='Missing id'):
        parse_builder_manifest(manifest, 'v2022')

    with manifest.open('w') as fp:
        dump_yaml({'id': 'org.test.Test'}, to=fp)

    with pytest.raises(ValueError, match='Missing runtime'):
        parse_builder_manifest(manifest, 'v2022')

    with manifest.open('w') as fp:
        dump_yaml({'id': 'org.test.Test', 'runtime': 'org.test.Platform'}, to=fp)

    with pytest.raises(ValueError, match='Missing sdk'):
        parse_builder_manifest(manifest, 'v2022')


def test_check_actions_for_duplicate_refs():
    check_actions_for_duplicate_refs(
        [
            BuildAction(
                'test1',
                Path(),
                BuildActionKind.FLATDEB_APP,
                frozenset({ArchlessRef(RefKind.RUNTIME, 'test', 'v2022')}),
            ),
            BuildAction(
                'test2',
                Path(),
                BuildActionKind.FLATDEB_APP,
                frozenset({ArchlessRef(RefKind.APP, 'test', 'v2022')}),
            ),
        ]
    )

    with pytest.raises(ValueError, match='Duplicate'):
        check_actions_for_duplicate_refs(
            [
                BuildAction(
                    'test1',
                    Path(),
                    BuildActionKind.FLATDEB_APP,
                    frozenset({ArchlessRef(RefKind.RUNTIME, 'test', 'v2022')}),
                ),
                BuildAction(
                    'test2',
                    Path(),
                    BuildActionKind.FLATPAK_BUILDER,
                    frozenset({ArchlessRef(RefKind.RUNTIME, 'test', 'v2022')}),
                ),
            ]
        )


def test_recursively_gather_deps():
    test1 = ArchlessRef(RefKind.RUNTIME, 'org.test.Platform', 'v2022')
    test2 = ArchlessRef(RefKind.APP, 'org.test.Test', 'v2022')
    test3 = ArchlessRef(RefKind.APP, 'org.test.Test2', 'v2022')

    test2_builder = BuildAction(
        'test',
        Path(),
        BuildActionKind.FLATDEB_APP,
        frozenset({test2}),
        needs_refs=frozenset({test1}),
    )

    test3_builder = BuildAction(
        'test',
        Path(),
        BuildActionKind.FLATDEB_APP,
        frozenset({test3}),
        needs_refs=frozenset({test2}),
    )

    deps = recursively_gather_deps(
        origin='test',
        refs=[test2],
        refs_to_actions={
            test2: test2_builder,
            test3: test3_builder,
        },
        arch=Arch.VALUES[0],
    )
    assert deps == {test2_builder: [test2]}

    deps = recursively_gather_deps(
        origin='test',
        refs=[test3],
        refs_to_actions={
            test2: test2_builder,
            test3: test3_builder,
        },
        arch=Arch.VALUES[0],
    )
    assert deps == {test2_builder: [test2], test3_builder: [test3]}


def test_recursively_gather_deps_unknown():
    test = ArchlessRef(RefKind.APP, 'org.test.Test', 'v2022')

    recursively_gather_deps(
        origin='test', refs=[test], refs_to_actions={}, arch=Arch.VALUES[0]
    )

    with pytest.raises(ValueError, match='unknown ref'):
        recursively_gather_deps(
            origin='test',
            refs=[test],
            refs_to_actions={},
            arch=Arch.VALUES[0],
            ignore_missing_deps=False,
        )
