#!/usr/bin/env python3

from dataclasses import dataclass
from pathlib import Path
from typing import IO, Any, ClassVar, Collection, FrozenSet, Iterable, Optional

import argparse
import collections
import contextlib
import dataclasses
import enum
import itertools
import os
import re
import shlex
import string
import sys

from ruamel.yaml import YAML
from ruamel.yaml.scalarstring import LiteralScalarString

import pydantic


def squish(text: str) -> str:
    return re.sub(r'\s+', ' ', text.strip())


FLATDEB_BUILDER_CONFIG_PATH = Path('flatdeb-builder.yaml')

FLATDEB_BUILDDIR = 'flatdeb-builddir'

DEFAULT_TIMEOUT = '4h'
DEFAULT_ARTIFACT_EXPIRATION = '1d'

SSH_ARGS = ['-oStrictHostKeyChecking=no']

DEPS_REMOTE_NAME = 'deps'

DBUS_START_COMMANDS = (
    'mkdir -p /run/dbus',
    'ln -s /run /var/run',
    squish(
        f"""
        dbus-send --system /org/freedesktop/DBus org.freedesktop.DBus
            || dbus-daemon --system --fork
        """
    ),
)

OSTREE_REBASE_SCRIPT = r"""
set -e

trap 'echo "\`$BASH_COMMAND\` exited with status $?" >&2' ERR

pushd {repo} > /dev/null
for ref in $refs; do
    echo "Rebasing $ref..."

    refpath="refs/heads/$ref"
    url={remote_url}"/$refpath"
    http_code=$(curl -Ls -o /dev/null --head -w "%{{http_code}}" "$url")
    case "$http_code" in
        200)
            # Move the old ref to a different name, so we can pull
            # down the commits on the remote end.
            prev="prev/$ref"
            prevpath="refs/heads/$prev"

            mkdir -p "$(dirname "$prevpath")"
            mv "$refpath" "$prevpath"

            echo "Pulling down remote version of $ref..."
            ostree pull \
                --depth=-1 \
                --mirror \
                --commit-metadata-only \
                --disable-fsync \
                origin "$ref"
            ostree show "$ref"

            message=$(ostree show "$prev" \
                        | grep '^ ' \
                        | sed 's/^ *//g')

            # Trick to preserve any trailing newlines:
            # https://unix.stackexchange.com/a/383411/117660
            # (The '|| :' at the end is because read always returns an exit code of 1 in
            # this case: https://stackoverflow.com/a/67066283/2097780)
            IFS= read -rd '' metadata < <(ostree cat "$prev" metadata) || :

            echo 'Applying new changes on top of remote...'
            ostree commit -b "$ref" -m "$message" \
                --tree=ref="$prev" \
                --add-metadata-string=xa.metadata="$metadata" \
                --bind-ref="$ref" \
                --fsync=false \
                --generate-sizes \
                {commit_args}
        ;;
        404)
            echo "No remote branch $ref found, skipping rebase."
        ;;
        *)
            echo "Error: Got HTTP '$http_code' trying to fetch '$url'"
            exit 1
        ;;
    esac
done
popd > /dev/null
        """.strip()


class noquote(str):
    pass


def shellquote(*args: str) -> str:
    return ' '.join(shlex.quote(a) if not isinstance(a, noquote) else a for a in args)


def escape_gitlab_variable(s: str) -> str:
    return s.replace('$', '$$')


@contextlib.contextmanager  # type: ignore
def wrap_exception_with_path(path: Path) -> Iterable[None]:
    try:
        yield
    except:
        raise ValueError(f'Failed to process {path}.')


class RefKind(enum.Enum):
    RUNTIME = enum.auto()
    APP = enum.auto()


@dataclass(frozen=True)
class Arch:
    debian_arch: str
    flatpak_arch: str
    needs_emulation: bool = False

    VALUES: ClassVar[list['Arch']] = []

    def job_name(self, name: str) -> str:
        return f'{name} ({self.flatpak_arch})'

    @classmethod
    def __get_validators__(cls) -> Iterable[Any]:
        yield cls.__validate

    @classmethod
    def __modify_schema__(cls, field_schema: Any) -> None:
        pass

    @classmethod
    def __validate(cls, value: Any) -> 'Arch':
        if not isinstance(value, str):
            raise TypeError('architecture must be a string')

        matching_arch = [a for a in cls.VALUES if a.flatpak_arch == value]
        if not matching_arch:
            all_valid_arch_names = ', '.join(a.flatpak_arch for a in cls.VALUES)
            raise TypeError(
                f"invalid architecture '{value}'"
                + f' (should be one of: {all_valid_arch_names})'
            )

        assert len(matching_arch) == 1
        return matching_arch[0]


Arch.VALUES = [
    Arch(debian_arch='amd64', flatpak_arch='x86_64'),
    Arch(debian_arch='arm64', flatpak_arch='aarch64', needs_emulation=True),
    Arch(debian_arch='armhf', flatpak_arch='arm', needs_emulation=True),
]


class FlatdebBuilderConfigModel(pydantic.BaseModel):
    class Config:
        extra = pydantic.Extra.forbid


class FlatdebBuilderConfig(FlatdebBuilderConfigModel):
    class StableBranches(enum.Enum):
        MAIN_ONLY = 'main_only'
        VERSIONED = 'versioned'

    class InstallDepsFrom(FlatdebBuilderConfigModel):
        class Sign(enum.Enum):
            ED25519 = 'ed25519'
            GPG = 'gpg'
            NONE = 'none'

        url: str
        sign: Sign = Sign.ED25519

    class TestJob(FlatdebBuilderConfigModel):
        class Targets(FlatdebBuilderConfigModel):
            runtimes: set[str] = set()
            apps: set[str] = set()

        command: str
        targets: Targets = Targets()

    class FlatpakRepo(FlatdebBuilderConfigModel):
        name: str
        template: str

        @staticmethod
        def _substitute_template(
            template: str,
            *,
            remote_url: str,
            public_key: Optional[str],
        ) -> str:
            return string.Template(template).substitute(
                REMOTE_URL=remote_url,
                FLATPAK_PUBLIC_KEY=public_key or '',
            )

        def substitute_template(
            self,
            *,
            remote_url: str,
            public_key: Optional[str],
        ) -> str:
            return self._substitute_template(
                self.template,
                remote_url=remote_url,
                public_key=public_key,
            )

        @pydantic.validator('template')
        def template_must_be_valid(cls, v: str) -> str:
            try:
                cls._substitute_template(v, public_key='', remote_url='')
            except KeyError as ex:
                raise ValueError(
                    f"template uses undefined variable '{ex.args[0]}'"
                ) from None

            return v

    supported_architectures: list[Arch] = Arch.VALUES
    stable_branches: StableBranches = StableBranches.MAIN_ONLY
    install_deps_from: Optional[InstallDepsFrom] = None
    tests: dict[str, TestJob] = {}
    flatpakrepo: Optional[FlatpakRepo] = None


@dataclass(frozen=True)
class ArchlessRef:
    kind: RefKind
    id: str
    branch: str

    def full_name(self, arch: Arch) -> str:
        return '/'.join(
            (self.kind.name.lower(), self.id, arch.flatpak_arch, self.branch)
        )


class BuildActionKind(enum.Enum):
    FLATDEB_RUNTIME = enum.auto()
    FLATDEB_APP = enum.auto()
    FLATPAK_BUILDER = enum.auto()

    @property
    def primary_ref_kind(self) -> RefKind:
        return RefKind.RUNTIME if self.is_runtime else RefKind.APP

    @property
    def is_flatdeb(self) -> bool:
        return self in {BuildActionKind.FLATDEB_RUNTIME, BuildActionKind.FLATDEB_APP}

    # For stage organization purposes, we assume anything flatpak-builder does
    # is an app.

    @property
    def is_runtime(self) -> bool:
        return self == BuildActionKind.FLATDEB_RUNTIME

    @property
    def is_app(self) -> bool:
        return self in {BuildActionKind.FLATDEB_APP, BuildActionKind.FLATPAK_BUILDER}


class PipelineStage(enum.Enum):
    BUILD_BASE = 'build base'
    BUILD_RUNTIMES = 'build runtimes'
    BUILD_APPS = 'build apps'
    TEST = 'test'
    PUBLISH = 'publish'


@dataclass(frozen=True)
class BuildAction:
    name: str
    manifest: Path
    kind: BuildActionKind
    known_outputs: FrozenSet[ArchlessRef]
    needs_refs: FrozenSet[ArchlessRef] = dataclasses.field(
        default_factory=lambda: frozenset()
    )

    def build_job_name(self, arch: Arch) -> str:
        return self._task_job_name('build', arch)

    def publish_job_name(self, arch: Arch) -> str:
        return self._task_job_name('publish', arch)

    def _task_job_name(self, task: str, arch: Arch) -> str:
        return arch.job_name(
            f'{task} {self.kind.primary_ref_kind.name.lower()} {self.name}'
        )

    def repo_dir(self) -> str:
        return '-'.join(
            (
                'repo',
                self.kind.primary_ref_kind.name.lower(),
                self.name,
            )
        )


def get_base_job_name(*, arch: Arch) -> str:
    return arch.job_name('build base')


def get_test_job_name(*, test: str, arch: Arch) -> str:
    return arch.job_name(f'test {test}')


def get_release_from_git_branch(branch: str) -> Optional[str]:
    BRANCH_RE = re.compile(r'\b(v\d{4}(?:dev\d|pre)?)\b')
    if (match := BRANCH_RE.search(branch)) is not None:
        return match.group(1)
    else:
        return None


@dataclass
class BranchInfo:
    is_stable: bool
    flatpak_branch: str
    os_release: str
    docker_image: str

    @staticmethod
    def from_env(stable_branches: FlatdebBuilderConfig.StableBranches) -> 'BranchInfo':
        mr_branch = os.environ.get('CI_MERGE_REQUEST_TARGET_BRANCH_NAME')
        target_branch = mr_branch or os.environ['CI_COMMIT_BRANCH']

        # Make sure that things like backport MRs from a stable branch won't
        # themselves be treated as stable builds.
        can_be_stable = mr_branch is None

        if stable_branches == FlatdebBuilderConfig.StableBranches.MAIN_ONLY:
            is_stable = can_be_stable and target_branch in {'main', 'master'}
            flatpak_branch = 'stable' if is_stable else 'test'
            os_release = os.environ['APERTIS_STABLE_RELEASE_DEFAULT']
        else:
            assert stable_branches == FlatdebBuilderConfig.StableBranches.VERSIONED

            os_release = flatpak_branch = (
                get_release_from_git_branch(target_branch)
                or os.environ['APERTIS_RELEASE_DEFAULT']
            )

            osname = os.environ['OSNAME']
            is_stable = can_be_stable and target_branch.startswith(f'{osname}/')

        template = os.environ['DOCKER_IMAGE_TEMPLATE']
        docker_image = template.format(release=os_release)

        return BranchInfo(
            is_stable=is_stable,
            flatpak_branch=flatpak_branch,
            os_release=os_release,
            docker_image=docker_image,
        )


@dataclass(frozen=True)
class UploadInfo:
    ssh_key: Path
    ssh_host: str
    ssh_port: Optional[int]
    remote_url: str
    root: str

    @staticmethod
    def from_env(
        *,
        is_legacy_ostree_push: bool,
        is_stable: bool,
    ) -> Optional['UploadInfo']:
        ssh_key = os.environ.get('ARCHIVE_SECRET_FILE')
        if ssh_key is None:
            return None

        ssh_host = os.environ['UPLOAD_HOST']

        ssh_port = None
        if ssh_port_s := os.environ.get('UPLOAD_PORT', ''):
            try:
                ssh_port = int(ssh_port_s)
            except ValueError:
                raise ValueError(f"Invalid SSH_PORT: '{ssh_port}'") from None

        env_variant = 'MAIN' if is_stable else 'TEST'
        remote_url = os.environ[f'REMOTE_URL_{env_variant}']
        root = os.environ[f'UPLOAD_ROOT_{env_variant}']

        return UploadInfo(
            ssh_key=Path(ssh_key),
            ssh_host=ssh_host,
            ssh_port=ssh_port,
            remote_url=remote_url,
            root=root,
        )

    @property
    def ssh_flags(self) -> list[str]:
        prefix = []
        if self.ssh_port is not None:
            prefix.append(f'-p{self.ssh_port}')

        prefix.extend(SSH_ARGS)
        return prefix

    @property
    def run_on_remote_host_command_prefix(self) -> list[str]:
        return [
            'ssh',
            *self.ssh_flags,
            self.ssh_host,
        ]

    @property
    def add_ssh_keys_commands(self) -> list[str]:
        return [
            shellquote('chmod', '0400', str(self.ssh_key)),
            'eval $(ssh-agent -s)',
            shellquote('ssh-add', str(self.ssh_key)),
        ]


@dataclass(frozen=True)
class OstreePush:
    is_legacy: bool
    config: Optional[str]

    @staticmethod
    def from_env(os_release: str) -> 'OstreePush':
        legacy_pattern = os.environ.get('OSTREE_PUSH_LEGACY_RELEASE_PATTERN')
        is_legacy = (
            legacy_pattern is not None
            and re.match(legacy_pattern, os_release) is not None
        )

        config = os.environ.get('OSTREE_PUSH_SERVER_CONFIG') if not is_legacy else None

        return OstreePush(
            is_legacy=is_legacy,
            config=config,
        )


@dataclass(frozen=True)
class SigningKeyPair:
    public: str
    private: str
    needs_sign_args: bool

    @staticmethod
    def from_env(*, is_legacy_ostree_push: bool) -> Optional['SigningKeyPair']:
        public = os.environ.get('FLATPAK_PUBLIC_KEY')
        private = os.environ.get('FLATPAK_PRIVATE_KEY')

        if public is None and private is None:
            return None
        elif public is None or private is None:
            raise ValueError(
                'FLATPAK_PUBLIC_KEY and FLATPAK_PRIVATE_KEY should both be set.'
            )
        else:
            return SigningKeyPair(
                public,
                private,
                # We don't sign things in the jobs with the newer ostree-push,
                # which rewrites and signs the commits from the server.
                needs_sign_args=is_legacy_ostree_push,
            )

    @staticmethod
    def get_sign_args(keypair: Optional['SigningKeyPair']) -> list[str]:
        if keypair and keypair.needs_sign_args:
            return [f'--sign={keypair.private}']
        else:
            return []


def get_flatdeb_command(
    *args: str,
    arch: Arch,
    branch: str,
    ostree_repo: Optional[str] = None,
) -> str:
    if flatdeb_repo := os.environ.get('FLATDEB_REPO'):
        flatdeb = os.path.join(flatdeb_repo, 'run.py')
    else:
        flatdeb = 'flatdeb'

    # NOTE: Both --build-area & --ostree-repo need to be absolute paths, due to
    # quirks with the way flatdeb uses them.

    command = [
        flatdeb,
        noquote(f'--build-area=$PWD/{shellquote(FLATDEB_BUILDDIR)}'),
        f'--suite={branch}',
        f'--arch={arch.debian_arch}',
    ]

    if ostree_repo is not None:
        command.append(noquote(f'--ostree-repo=$PWD/{shellquote(ostree_repo)}'))

    for arg in args:
        if arg == 'app':
            command += [
                f'--runtime-branch={branch}',
                'app',
                f'--app-branch={branch}',
            ]
        else:
            command.append(arg)

    return shellquote(*command)


@dataclass(frozen=True)
class FlatdebManifestInfo:
    id_prefix: str
    refs: FrozenSet[ArchlessRef]


def parse_flatdeb_manifest(manifest: Path, branch: str) -> FlatdebManifestInfo:
    print(f'Processing flatdeb runtime manifest {manifest}...', file=sys.stderr)

    with manifest.open() as fp:
        contents = YAML(typ='safe').load(fp)

    id_prefix = contents.get('id_prefix')
    if not id_prefix:
        raise ValueError('Missing id_prefix key.')

    refs = frozenset(
        {
            ArchlessRef(kind=RefKind.RUNTIME, id=f'{id_prefix}.{suffix}', branch=branch)
            for suffix in ('Platform', 'Sdk')
        }
    )

    return FlatdebManifestInfo(
        id_prefix=id_prefix,
        refs=refs,
    )


@dataclass(frozen=True)
class BuilderManifestInfo:
    ref: ArchlessRef
    deps: FrozenSet[ArchlessRef]


def parse_builder_manifest(manifest: Path, branch: str) -> BuilderManifestInfo:
    print(f'Processing flatpak-builder manifest {manifest}...', file=sys.stderr)

    with manifest.open() as fp:
        contents = YAML(typ='safe').load(fp)

    app_id = contents.get('app-id') or contents.get('id')
    if not app_id:
        raise ValueError('Missing id/app-id key.')

    if contents.get('build-extension', False):
        ref_kind = RefKind.RUNTIME
    else:
        ref_kind = RefKind.APP

    ref = ArchlessRef(kind=ref_kind, id=app_id, branch=branch)
    deps = set()

    runtime = contents.get('runtime')
    if not runtime:
        raise ValueError('Missing runtime key.')

    sdk = contents.get('sdk')
    if not sdk:
        raise ValueError('Missing sdk key.')

    runtime_version = contents.get('runtime-version') or branch

    # TODO: parse sdk values with a branch at the end
    # TODO: parse SDK extensions

    deps.add(ArchlessRef(kind=RefKind.RUNTIME, id=runtime, branch=runtime_version))
    deps.add(ArchlessRef(kind=RefKind.RUNTIME, id=sdk, branch=runtime_version))

    return BuilderManifestInfo(ref=ref, deps=frozenset(deps))


def find_build_actions(root: Path, branch: str) -> Iterable[BuildAction]:
    found_flatdeb_runtime = False

    for manifest in sorted(root.glob('runtimes/*.yaml')):
        with wrap_exception_with_path(manifest):
            if not found_flatdeb_runtime:
                print(
                    'Found flatdeb runtime: assuming flatdeb source layout...',
                    file=sys.stderr,
                )

            found_flatdeb_runtime = True

            flatdeb_info = parse_flatdeb_manifest(manifest, branch)
            yield BuildAction(
                name=flatdeb_info.id_prefix,
                manifest=manifest,
                kind=BuildActionKind.FLATDEB_RUNTIME,
                known_outputs=flatdeb_info.refs,
            )

    if found_flatdeb_runtime:
        for manifest in sorted(root.glob('apps/*.yaml')):
            with wrap_exception_with_path(manifest):
                builder_info = parse_builder_manifest(manifest, branch)
                yield BuildAction(
                    name=builder_info.ref.id,
                    manifest=manifest,
                    kind=BuildActionKind.FLATDEB_APP,
                    known_outputs=frozenset({builder_info.ref}),
                    needs_refs=builder_info.deps,
                )
    else:
        print(
            'No flatdeb runtimes found: assuming flatpak-builder source layout...',
            file=sys.stderr,
        )

        for manifest in sorted(root.glob('*.yaml')):
            # Since we're pulling all yaml files in the root, make sure we skip
            # any hidden ones + our own configuration.
            if (
                manifest.name.startswith('.')
                or manifest.name == FLATDEB_BUILDER_CONFIG_PATH.name
            ):
                continue

            with wrap_exception_with_path(manifest):
                builder_info = parse_builder_manifest(manifest, branch)
                yield BuildAction(
                    name=builder_info.ref.id,
                    manifest=manifest,
                    kind=BuildActionKind.FLATPAK_BUILDER,
                    known_outputs=frozenset({builder_info.ref}),
                    needs_refs=builder_info.deps,
                )


def check_actions_for_duplicate_refs(actions: Collection[BuildAction]) -> None:
    refs_to_actions: dict[ArchlessRef, BuildAction] = {}

    for action in actions:
        for ref in action.known_outputs:
            if (previous_action := refs_to_actions.get(ref)) is not None:
                raise ValueError(
                    f'Duplicate ref {ref} from {previous_action.manifest}'
                    f' and {action.manifest}'
                )

            refs_to_actions[ref] = action


def actions_by_outputs(
    actions: Collection[BuildAction],
) -> dict[ArchlessRef, BuildAction]:
    return {out: action for action in actions for out in action.known_outputs}


def merge_action_repos(
    *,
    dest: str,
    source_actions_to_refs: dict[BuildAction, list[ArchlessRef]],
    arch: Arch,
) -> list[str]:
    script = [
        shellquote(
            'ostree',
            f'--repo={dest}',
            'init',
            '--mode=archive-z2',
        ),
    ]

    for source, refs in source_actions_to_refs.items():
        source_repo = source.repo_dir()
        script += [
            shellquote(
                'ostree',
                f'--repo={dest}',
                'remote',
                'add',
                '--no-gpg-verify',
                source_repo,
                noquote(f'"file://$PWD/"{shellquote(source_repo)}'),
            ),
            shellquote(
                'ostree',
                f'--repo={dest}',
                'pull',
                '--mirror',
                source_repo,
                *(ref.full_name(arch) for ref in refs),
            ),
        ]

    script += [
        shellquote(
            'flatpak',
            'build-update-repo',
            '--no-update-appstream',
            dest,
        ),
    ]

    return script


def get_deps_remote_command(
    install_deps_from: FlatdebBuilderConfig.InstallDepsFrom,
) -> str:
    no_sign_flags = (
        ['--no-sign-verify']
        if install_deps_from.sign != FlatdebBuilderConfig.InstallDepsFrom.Sign.ED25519
        else []
    )

    return shellquote(
        'flatpak',
        'remote-add',
        '--user',
        *no_sign_flags,
        DEPS_REMOTE_NAME,
        install_deps_from.url,
    )


def generate_job(
    stage: PipelineStage,
    script: list[str],
    *,
    variables: dict[str, str] = {},
    before_script: Optional[list[str]] = None,
    after_script: Optional[list[str]] = None,
    run_as_arch: Optional[Arch] = None,
    needs: Optional[list[str]] = None,
    artifact_paths: list[str] = [],
    tags: list[str] = [],
    resource_group: Optional[str] = None,
) -> dict[str, Any]:
    if run_as_arch is not None and run_as_arch.needs_emulation:
        script = [
            f'/usr/sbin/update-binfmts --enable qemu-{run_as_arch.flatpak_arch}',
            *script,
        ]

        if (tag := os.environ.get('QEMU_RUNNER_TAG')) is not None:
            tags = [*tags, tag]

    job: dict[str, Any] = {
        'stage': stage.value,
        'script': script,
        'timeout': DEFAULT_TIMEOUT,
    }

    if tags:
        job['tags'] = tags

    if resource_group is not None:
        job['resource_group'] = resource_group

    job['variables'] = dict(variables, GIT_DEPTH=1)

    for key, extra_script in (
        ('before_script', before_script),
        ('after_script', after_script),
    ):
        if extra_script is not None:
            job[key] = extra_script

    if needs is not None:
        # Make sure to de-dup the value, but re-sort to keep the output deterministic.
        job['needs'] = sorted(set(needs))

    if artifact_paths:
        job['artifacts'] = {
            'expire_in': DEFAULT_ARTIFACT_EXPIRATION,
            'paths': artifact_paths,
        }

    return job


def generate_base_job(*, arch: Arch, branch: str) -> dict[str, Any]:
    return generate_job(
        PipelineStage.BUILD_BASE,
        [get_flatdeb_command('base', arch=arch, branch=branch)],
        run_as_arch=arch,
        artifact_paths=[f'{FLATDEB_BUILDDIR}/base-{branch}-{arch.debian_arch}.tar.gz'],
    )


def generate_build_runtime_job(
    action: BuildAction,
    *,
    keypair: Optional[SigningKeyPair],
    arch: Arch,
    branch: str,
) -> dict[str, Any]:
    assert not action.needs_refs
    repo = action.repo_dir()

    return generate_job(
        PipelineStage.BUILD_RUNTIMES,
        [
            get_flatdeb_command(
                *SigningKeyPair.get_sign_args(keypair),
                '--export-bundles',
                'runtimes',
                str(action.manifest),
                arch=arch,
                branch=branch,
                ostree_repo=repo,
            ),
        ],
        run_as_arch=arch,
        needs=[get_base_job_name(arch=arch)],
        artifact_paths=[
            f'{FLATDEB_BUILDDIR}/*.flatpak',
            repo,
        ],
    )


def generate_build_app_job(
    action: BuildAction,
    *,
    keypair: Optional[SigningKeyPair],
    depends: dict[BuildAction, list[ArchlessRef]],
    arch: Arch,
    branch: str,
    install_deps_from: Optional[FlatdebBuilderConfig.InstallDepsFrom],
) -> dict[str, Any]:
    DEP_REFS_FILE = 'dep-refs-to-remove'

    # For now, we only support deps from other manifests for flatdeb apps, since
    # that's used there to depend on the runtimes we just built.
    assert (action.kind == BuildActionKind.FLATDEB_APP) == bool(depends)

    repo = action.repo_dir()

    script = merge_action_repos(
        dest=repo,
        source_actions_to_refs=depends,
        arch=arch,
    )

    if depends:
        # We need to figure out what refs were added from dependencies & remove
        # those, so save the list of refs that came from deps.
        script += [
            f'find {shellquote(repo)}/refs/heads -type f -fprint {DEP_REFS_FILE}',
        ]

    if action.kind.is_flatdeb:
        script.append(
            get_flatdeb_command(
                *SigningKeyPair.get_sign_args(keypair),
                '--export-bundles',
                'app',
                '--no-fakeroot',
                str(action.manifest),
                arch=arch,
                branch=branch,
                ostree_repo=repo,
            )
        )
    else:
        args = [
            '--force-clean',
            f'--arch={arch.flatpak_arch}',
            '--bundle-sources',
            '--disable-rofiles-fuse',
            f'--default-branch={branch}',
            f'--repo={repo}',
            '--user',
            *SigningKeyPair.get_sign_args(keypair),
        ]

        if install_deps_from is not None:
            script.append(get_deps_remote_command(install_deps_from))
            args.append(f'--install-deps-from={DEPS_REMOTE_NAME}')

        script.append(
            shellquote(
                'flatpak-builder',
                *args,
                '_build',
                str(action.manifest),
            )
        )

    if depends:
        script += [
            f'xargs -ra {DEP_REFS_FILE} rm',
        ]

    return generate_job(
        PipelineStage.BUILD_APPS,
        [*DBUS_START_COMMANDS, *script],
        run_as_arch=arch,
        needs=sorted({dep.build_job_name(arch) for dep in depends}),
        artifact_paths=[
            f'{FLATDEB_BUILDDIR}/*.flatpak',
            repo,
        ],
    )


def generate_test_job(
    job: FlatdebBuilderConfig.TestJob,
    *,
    depends: dict[BuildAction, list[ArchlessRef]],
    branch: str,
    arch: Arch,
    install_deps_from: Optional[FlatdebBuilderConfig.InstallDepsFrom],
) -> dict[str, Any]:
    TEST_REPO = 'test-repo'

    merge_script = merge_action_repos(
        dest=TEST_REPO,
        # Make sure we mirror *all* the refs.
        source_actions_to_refs={action: [] for action in depends.keys()},
        arch=arch,
    )

    if install_deps_from is not None:
        merge_script.append(get_deps_remote_command(install_deps_from))

    return generate_job(
        PipelineStage.TEST,
        [*DBUS_START_COMMANDS, *merge_script, job.command],
        variables={
            'OSNAME': os.environ['OSNAME'],
            'FLATPAK_BRANCH': branch,
            'FLATPAK_ARCH': arch.flatpak_arch,
            'DEBIAN_ARCH': arch.debian_arch,
            'TARGETS_REPO': TEST_REPO,
        },
        run_as_arch=arch,
        needs=sorted({dep.build_job_name(arch) for dep in depends}),
    )


def generate_publish_job(
    action: BuildAction,
    *,
    ostree_push: OstreePush,
    keypair: Optional[SigningKeyPair],
    upload_info: UploadInfo,
    arch: Arch,
    after_jobs: list[str] = [],
) -> dict[str, Any]:
    repo = action.repo_dir()
    q_repo = shellquote(repo)

    push_script = [
        f'refs=$(ostree refs --repo={q_repo} --list runtime app)',
        shellquote(
            *upload_info.run_on_remote_host_command_prefix,
            'mkdir',
            '-p',
            upload_info.root,
        ),
        shellquote(
            *upload_info.run_on_remote_host_command_prefix,
            'ostree',
            'init',
            f'--repo={upload_info.root}',
            '--mode=archive-z2',
        ),
    ]

    host = upload_info.ssh_host
    if upload_info.ssh_port is not None:
        host += f':{upload_info.ssh_port}'
    push_url = f'ssh://{host}/{upload_info.root.removeprefix("/")}'

    if ostree_push.is_legacy:
        signing_args = SigningKeyPair.get_sign_args(keypair)

        push_script += [
            shellquote(
                'ostree',
                'remote',
                'add',
                f'--repo={repo}',
                '--no-gpg-verify',
                '--if-not-exists',
                'origin',
                upload_info.remote_url,
            ),
            LiteralScalarString(
                OSTREE_REBASE_SCRIPT.format(
                    repo=q_repo,
                    remote_url=shellquote(upload_info.remote_url),
                    commit_args=shellquote(*signing_args),
                )
            ),
            f'echo "Pushing refs: $refs"',
            shellquote(
                'ostree-push',
                f'--repo={repo}',
                push_url,
                noquote('$refs'),
            ),
        ]

        # Update the remote repo in the after_script, so that, no matter what happens,
        # the remote summary won't get out of date. (An out-of-date remote summary will
        # break the rebase operation and thus make it impossible to push until the
        # summary is re-generated. We only need this for the legacy ostree-push; the
        # newer version doesn't need an explicit rebase, which means that a
        # timed out / cancelled publish won't break all other publishes.)
        after_script = [
            # Need to re-add the public key here, because ssh-agent stopped already:
            # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1926
            *upload_info.add_ssh_keys_commands,
            shellquote(
                *upload_info.run_on_remote_host_command_prefix,
                'flatpak',
                'build-update-repo',
                *signing_args,
                upload_info.root,
            ),
        ]
    else:
        if ostree_push.config is not None:
            config_args = [f'-oSetEnv=OSTREE_RECEIVE_CONF={ostree_push.config}']
        else:
            config_args = []

        host = upload_info.ssh_host
        if upload_info.ssh_port is not None:
            host += f':{upload_info.ssh_port}'

        push_script += [
            f'echo "Pushing refs: $refs"',
            shellquote(
                'ostree-push',
                *config_args,
                f'--repo={repo}',
                push_url,
                noquote('$refs'),
            ),
        ]
        after_script = None

    return generate_job(
        PipelineStage.PUBLISH,
        push_script,
        tags=['lightweight'],
        before_script=upload_info.add_ssh_keys_commands,
        # Update the remote repo in the after_script, so that, no matter what happens,
        # the remote refs won't get out of date.
        after_script=after_script,
        needs=[action.build_job_name(arch), *after_jobs],
        resource_group='publish-$CI_COMMIT_REF_NAME',
        # Note that we intentionally don't set run_as_arch=, because publishing does not
        # need any emulation for the target architecture; everything we run is
        # host-native.
    )


def generate_publish_flatpakrepo_job(
    *,
    config: FlatdebBuilderConfig.FlatpakRepo,
    keypair: Optional[SigningKeyPair],
    upload_info: UploadInfo,
) -> dict[str, Any]:
    FLATPAKREPO_VAR = 'FLATPAKREPO'

    flatpakrepo = escape_gitlab_variable(
        config.substitute_template(
            remote_url=upload_info.remote_url,
            public_key=keypair.public if keypair is not None else None,
        )
    )

    return generate_job(
        PipelineStage.PUBLISH,
        [
            *upload_info.add_ssh_keys_commands,
            shellquote(
                *upload_info.run_on_remote_host_command_prefix,
                'mkdir',
                '-p',
                upload_info.root,
            ),
            f'echo "${FLATPAKREPO_VAR}" > repo.flatpakrepo',
            shellquote(
                'rsync',
                '-e',
                shellquote('ssh', *upload_info.ssh_flags),
                '-arvc',
                'repo.flatpakrepo',
                f'{upload_info.ssh_host}:{upload_info.root}/{config.name}.flatpakrepo',
            ),
        ],
        tags=['lightweight'],
        variables={FLATPAKREPO_VAR: LiteralScalarString(flatpakrepo)},
        needs=[],
    )


def recursively_gather_deps(
    *,
    origin: str,
    refs: Iterable[ArchlessRef],
    refs_to_actions: dict[ArchlessRef, BuildAction],
    arch: Arch,
    ignore_missing_deps: bool = True,
) -> dict[BuildAction, list[ArchlessRef]]:
    seen: set[ArchlessRef] = set()
    deps = collections.defaultdict(lambda: [])

    def gather(refs: Iterable[ArchlessRef]) -> None:
        # Sort the refs so that the order is deterministic.
        for ref in sorted(set(refs) - seen, key=lambda ref: ref.full_name(arch)):
            seen.add(ref)

            dep = refs_to_actions.get(ref)
            if dep is None:
                if not ignore_missing_deps:
                    raise ValueError(
                        f'{origin} depends on unknown ref {ref.full_name(arch)}'
                    )
                continue

            deps[dep].append(ref)
            gather(dep.needs_refs)

    gather(refs)
    return deps


def generate_pipeline(
    *,
    branch_info: BranchInfo,
    upload_info: Optional[UploadInfo],
    ostree_push: OstreePush,
    keypair: Optional[SigningKeyPair],
    actions: Collection[BuildAction],
    config: FlatdebBuilderConfig,
) -> dict[str, Any]:
    branch = branch_info.flatpak_branch
    pipeline: dict[str, Any] = {}

    # This needs to be repeated in here *and* ci-flatdeb-builder.yaml to avoid errors:
    # https://gitlab.com/gitlab-org/gitlab/-/issues/276179#note_700305979
    pipeline['workflow'] = {
        'rules': [
            {
                'if': squish(
                    r"""$CI_COMMIT_BRANCH
                    && $CI_COMMIT_BRANCH !~ /^apertis\//
                    && $CI_COMMIT_BRANCH != 'main'
                    && $CI_COMMIT_BRANCH != 'master'
                    && $CI_OPEN_MERGE_REQUESTS"""
                ),
                'when': 'never',
            },
            {
                'when': 'always',
            },
        ]
    }

    pipeline['image'] = branch_info.docker_image

    refs_to_actions = actions_by_outputs(actions)
    runtime_actions = [action for action in actions if action.kind.is_runtime]
    app_actions = [action for action in actions if action.kind.is_app]

    pipeline['stages'] = stages = []
    if runtime_actions:
        stages.append(PipelineStage.BUILD_BASE.value)
        stages.append(PipelineStage.BUILD_RUNTIMES.value)
    if app_actions:
        stages.append(PipelineStage.BUILD_APPS.value)
    if config.tests:
        stages.append(PipelineStage.TEST.value)
    stages.append(PipelineStage.PUBLISH.value)

    if runtime_actions:
        for arch in config.supported_architectures:
            pipeline[get_base_job_name(arch=arch)] = generate_base_job(
                arch=arch, branch=branch
            )

    for action, arch in itertools.product(
        runtime_actions, config.supported_architectures
    ):
        assert not action.needs_refs
        pipeline[action.build_job_name(arch)] = generate_build_runtime_job(
            action,
            keypair=keypair,
            arch=arch,
            branch=branch,
        )

    for action, arch in itertools.product(app_actions, config.supported_architectures):
        can_install_deps = (
            not action.kind.is_flatdeb and config.install_deps_from is not None
        )
        depends = recursively_gather_deps(
            origin=action.manifest.name,
            refs=action.needs_refs,
            refs_to_actions=refs_to_actions,
            arch=arch,
            ignore_missing_deps=can_install_deps,
        )

        pipeline[action.build_job_name(arch)] = generate_build_app_job(
            action,
            keypair=keypair,
            depends=depends,
            arch=arch,
            branch=branch,
            install_deps_from=config.install_deps_from,
        )

    test_jobs = collections.defaultdict(lambda: set())
    for test_name, test in config.tests.items():
        for arch in config.supported_architectures:
            targets = [
                *(
                    ArchlessRef(kind=RefKind.RUNTIME, id=dep, branch=branch)
                    for dep in test.targets.runtimes
                ),
                *(
                    ArchlessRef(kind=RefKind.APP, id=dep, branch=branch)
                    for dep in test.targets.apps
                ),
            ]

            depends = recursively_gather_deps(
                origin='test job',
                refs=targets,
                refs_to_actions=refs_to_actions,
                arch=arch,
                ignore_missing_deps=config.install_deps_from is not None,
            )

            test_job_name = get_test_job_name(test=test_name, arch=arch)

            for target in targets:
                test_jobs[target].add(test_job_name)

            pipeline[test_job_name] = generate_test_job(
                test,
                depends=depends,
                branch=branch,
                arch=arch,
                install_deps_from=config.install_deps_from,
            )

    if upload_info is not None:
        for action, arch in itertools.product(
            itertools.chain(runtime_actions, app_actions),
            config.supported_architectures,
        ):
            pipeline[action.publish_job_name(arch)] = generate_publish_job(
                action,
                ostree_push=ostree_push,
                keypair=keypair,
                upload_info=upload_info,
                arch=arch,
                after_jobs=sorted(
                    itertools.chain.from_iterable(
                        test_jobs[ref] for ref in action.known_outputs
                    )
                ),
            )

        if config.flatpakrepo is not None:
            pipeline['publish flatpakrepo'] = generate_publish_flatpakrepo_job(
                config=config.flatpakrepo,
                keypair=keypair,
                upload_info=upload_info,
            )

    return pipeline


def dump_yaml(data: Any, *, to: IO[str]) -> None:
    yaml = YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)
    yaml.dump(data, to)


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', default='-')
    args = parser.parse_args()

    if FLATDEB_BUILDER_CONFIG_PATH.exists():
        print(f'Loading {FLATDEB_BUILDER_CONFIG_PATH}...', file=sys.stderr)
        with FLATDEB_BUILDER_CONFIG_PATH.open() as fp:
            config_data = YAML(typ='safe').load(fp)
            config = FlatdebBuilderConfig.parse_obj(config_data)
    else:
        print(f'{FLATDEB_BUILDER_CONFIG_PATH} not found; using defaults')
        config = FlatdebBuilderConfig()

    branch_info = BranchInfo.from_env(config.stable_branches)
    ostree_push = OstreePush.from_env(os_release=branch_info.os_release)
    upload_info = UploadInfo.from_env(
        is_legacy_ostree_push=ostree_push.is_legacy,
        is_stable=branch_info.is_stable,
    )
    keypair = SigningKeyPair.from_env(is_legacy_ostree_push=ostree_push.is_legacy)

    actions = list(find_build_actions(Path('.'), branch_info.flatpak_branch))

    check_actions_for_duplicate_refs(actions)

    pipeline = generate_pipeline(
        branch_info=branch_info,
        upload_info=upload_info,
        ostree_push=ostree_push,
        keypair=keypair,
        actions=actions,
        config=config,
    )

    if args.output == '-':
        dump_yaml(pipeline, to=sys.stdout)
    else:
        with open(args.output, 'w') as fp:
            dump_yaml(pipeline, to=fp)


if __name__ == '__main__':
    main()
