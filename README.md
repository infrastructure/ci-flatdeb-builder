# ci-flatdeb-builder

This repository contains GitLab pipelines and generators to build flatdeb and
flatpak-builder projects for Apertis.

## Local Development Environment

Create a local virtualenv for this project, then simply `pip3 install -r
requirements-dev.txt` to install all of the development dependencies.

## Running Tests

ci-flatdeb-builder has three types of tests:

- Local unit tests for various helpers in the pipeline generator, in
  `tests/test_helpers.py`.
- Approval tests that check to ensure the output of ci-flatdeb-builder has not changed,
  in `tests/test_approval.py` and `tests/approved_Files`.
- Integration-style tests that run a full pipeline on GitLab and then examine the
  results, in `tests/test_build_pipelines.py`.

The integration-style tests are marked with the `gitlab` marker, thus you can
exclusively run them via `pytest -m gitlab`, or run everything *except* for them via
`pytest -m 'not gitlab'`.

### Approval Tests

If the ci-flatdeb-builder output changes for any reason, the approval tests will likely
start failing. You can run `tests/approval_all.sh` to mark all of the new, changed
results as approved, then `git diff tests/approved_files` to easily confirm that the
changes are as expected.

### GitLab Tests

In order to run the tests marked `gitlab`, you must provide a GitLab pipeline to
examine:

- If you want to trigger a new pipeline run automatically, pass `--pipeline-id trigger`.
- If you instead want to run the tests on the results of a pipeline that already ran,
  pass `--pipeline-id PIPELINE-ID`.

Note that, in either case, the build results examined from images.apertis.org will be
those resulting from the *latest* build of the current branch.

By default, the tests will upload their artifacts to
`https://images.apertis.org/test/ci-flatdeb-builder-SLUG`, where `SLUG` is your current
branch name with all non-alphanumeric characters replaced with dashes (`-`).

#### Test Repositories

There are four different pipelines run:

- Two on a [repository containing demo flatdeb runtimes](TODO), both marked with the
  `gitlab_repo_runtimes` pytest marker:
  - One for the stable `apertis/v2022` Git branch, marked with the `gitlab_branch_main`
    marker.
  - One for the `wip/fake/test` Git branch, marked with the `gitlab_branch_test` marker.
- Two on a [repository containing demo flatpak-builder applications](TODO), both marked
  with the `gitlab_repo_apps` marker:
  - One for the stable `main` Git branch, marked with the `gitlab_branch_main`
    marker.
  - One for the `wip/fake/test` Git branch, marked with the `gitlab_branch_test` marker.

In order to run only some of these, you can pass their markers to `pytest -m`, e.g.
`pytest -m 'gitlab and gitlab_repo_runtimes and gitlab_branch_main'` will run the test
for the runtimes repo's stable branch, and `pytest -m 'gitlab and gitlab_repo_apps'`
will run the two tests for the apps repo.

#### GitLab Instance Credentials

The credentials used to connect to the Apertis GitLab instance will, by default, be
taken from [the standard python-gitlab configuration
file](https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files),
with the server section name "apertis". To change the section name, pass
`--gitlab-instance NAME` to pytest.
